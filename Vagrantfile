# -*- mode: ruby -*-
# # vi: set ft=ruby :

require 'fileutils'

Vagrant.require_version ">= 1.6.0"

CONFIG = File.join(File.dirname(__FILE__), "config.rb")
CLOUD_INIT = File.join(File.dirname(__FILE__), "user-data")
CLOUD_INIT_PATH = File.join(File.dirname(__FILE__), "/.cloud-init")

# DO NOT MODIFY! Instead, create a config.rb file in the same directory as
# this one with your variable overrides set. Copy the contents of
# config.rb.sample as a starting point.
$num_managers = 1
$num_nodes = 1
$update_channel = "beta"
$image_version = "current"
$enable_serial_logging = false
$share_home = false
$vm_gui = false
$vm_memory = 1024
$vm_cpus = 1
$shared_folders = {"data" => "/data"}
$forwarded_ports = {}
$ip_prefix = "172.8.8."
$public_containers = ["node-01"]

if File.exist?(CONFIG)
  require CONFIG
end

Vagrant.configure("2") do |config|
  # always use Vagrants insecure key
  config.ssh.insert_key = false

  config.vm.box = "coreos-%s" % $update_channel
  if $image_version != "current"
    config.vm.box_version = $image_version
  end
  config.vm.box_url = "https://storage.googleapis.com/%s.release.core-os.net/amd64-usr/%s/coreos_production_vagrant.json" % [$update_channel, $image_version]

  ["vmware_fusion", "vmware_workstation"].each do |vmware|
    config.vm.provider vmware do |v, override|
      override.vm.box_url = "https://storage.googleapis.com/%s.release.core-os.net/amd64-usr/%s/coreos_production_vagrant_vmware_fusion.json" % [$update_channel, $image_version]
    end
  end

  config.vm.provider :virtualbox do |v|
    # On VirtualBox, we don't have guest additions or a functional vboxsf
    # in CoreOS, so tell Vagrant that so it can be smarter.
    v.check_guest_additions = false
    v.functional_vboxsf     = false
  end

  # plugin conflict
  if Vagrant.has_plugin?("vagrant-vbguest") then
    config.vbguest.auto_update = false
  end

  (1..$num_managers + $num_nodes).each do |i|
    instance_postfix = i > $num_managers ? i - $num_managers : i
    instance_name_prefix = i > $num_managers ? 'node' : 'manager'

    config.vm.define vm_name = "%s-%02d" % [instance_name_prefix, instance_postfix] do |config|
      config.vm.hostname = vm_name

      if $enable_serial_logging
        logdir = File.join(File.dirname(__FILE__), "log")
        FileUtils.mkdir_p(logdir)

        serialFile = File.join(logdir, "%s-serial.txt" % vm_name)
        FileUtils.touch(serialFile)

        ["vmware_fusion", "vmware_workstation"].each do |vmware|
          config.vm.provider vmware do |v, override|
            v.vmx["serial0.present"] = "TRUE"
            v.vmx["serial0.fileType"] = "file"
            v.vmx["serial0.fileName"] = serialFile
            v.vmx["serial0.tryNoRxLoss"] = "FALSE"
          end
        end

        config.vm.provider :virtualbox do |vb, override|
          vb.customize ["modifyvm", :id, "--uart1", "0x3F8", "4"]
          vb.customize ["modifyvm", :id, "--uartmode1", serialFile]
        end
      end

      if $expose_docker_tcp
        config.vm.network "forwarded_port", guest: 2375, host: ($expose_docker_tcp + i - 1), auto_correct: true
      end

      $forwarded_ports.each do |guest, host|
        config.vm.network "forwarded_port", guest: guest, host: host, auto_correct: true
      end

      ["vmware_fusion", "vmware_workstation"].each do |vmware|
        config.vm.provider vmware do |v|
          v.gui = $vm_gui
          v.vmx['memsize'] = $vm_memory
          v.vmx['numvcpus'] = $vm_cpus
        end
      end

      config.vm.provider :virtualbox do |vb|
        vb.gui = $vm_gui
        vb.memory = $vm_memory
        vb.cpus = $vm_cpus
      end

      ip = $ip_prefix + "#{i+100}"
      config.vm.network :private_network, ip: ip

      # Uncomment below to enable NFS for sharing the host machine into the coreos-vagrant VM.
      #config.vm.synced_folder ".", "/home/core/share", id: "core", :nfs => true, :mount_options => ['nolock,vers=3,udp']
      $shared_folders.each_with_index do |(host_folder, guest_folder), index|
        config.vm.synced_folder host_folder.to_s, guest_folder.to_s, id: "core-share%02d" % index, nfs: true, mount_options: ['nolock,vers=3,udp']
      end

      if $share_home
        config.vm.synced_folder ENV['HOME'], ENV['HOME'], id: "home", :nfs => true, :mount_options => ['nolock,vers=3,udp']
      end

      if ARGV[0].eql?('up')
        require 'open-uri'
        require 'yaml'

        data = YAML.load(IO.readlines(CLOUD_INIT)[1..-1].join)

        (0..data['coreos']['units'].length-1).each do |u|
          if instance_name_prefix == 'manager' and (data['coreos']['units'][u]['name'].end_with? '-manager.service' or data['coreos']['units'][u]['name'].start_with? 'shipyard')
            data['coreos']['units'][u]['enable'] = true
            data['coreos']['units'][u]['command'] = 'start'
          end

          if instance_name_prefix == 'node' and data['coreos']['units'][u]['name'].end_with? '-agent.service'
            data['coreos']['units'][u]['enable'] = true
            data['coreos']['units'][u]['command'] = 'start'
          end

          if data['coreos']['units'][u]['name'] == 'docker.service' and $public_containers.include? vm_name
            data['coreos']['units'][u]['drop-ins'][0]['content'] = data['coreos']['units'][u]['drop-ins'][0]['content'].sub! 'internal', 'public'
          end
        end

        yaml = YAML.dump(data)
        compiled_user_data = File.join(CLOUD_INIT_PATH, "user-data.#{instance_name_prefix}")
        FileUtils.mkdir_p CLOUD_INIT_PATH
        File.open(compiled_user_data, 'w') { |file| file.write("#cloud-config\n\n#{yaml}") }

        config.vm.provision :file, :source => "#{compiled_user_data}", :destination => "/tmp/vagrantfile-user-data"
        config.vm.provision :shell, :inline => "mv /tmp/vagrantfile-user-data /var/lib/coreos-vagrant/", :privileged => true
      end

    end
  end
end