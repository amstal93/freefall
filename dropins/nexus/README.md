Nexus Dropin
============
The main use-case for this dropin is to provide a caching layer for the public docker registry (docker hub) and a private repository for custom images used throughout the cluster.

**PLEASE NOTE** - this dropin requires a minimum of 1200mb of memory on the node it is launched on.  If you are running this on Vagrant it might be an idea to override the configuration.

Installation
------------
- Run `$ docker-compose up -d`
- Check which node nexus was installed on at `http://manager-01:8080` or with `docker ps -f name=nexus`.
- Login to nexus `http://<NODE_HOST>:5500` with username `admin` and password `admin123`.

Setup
-----

Mirror of public docker hub repository (for caching):
- Create Repository: `docker (proxy)`
- Name: `docker-hub`
- Enable Docker V1 API: `true`
- Remote Storage: `https://registry-1.docker.io`
- Docker Index: `Use Docker Hub`

Private docker repository:
- Create Repository: `docker (hosted)`
- Name: `docker-local`
- Repository Connectors (https): `5050`

Group to pull from all docker repositories:
- Create Repository: `docker (group)`
- Name: `docker-public`
- Repository Connectors (https): `5000`
- Group: `docker-hub` `docker-local`