FreeFall PaaS
=============
A highly opinionated vagrant cluster setup for consul service discovery and swarm orchestration.

Features
--------
- Hashicorp Consul: for service discovery, health checks and a distributed key/value store.
- Docker Swarm: for command line container orchestration.
- Shipyard: for a unified docker container ui across the cluster.

Quickstart
----------
- Install [Docker Toolbox](https://www.docker.com/)
- Install [Vagrant](https://www.vagrantup.com/)
- Install [vagrant-hostsupdater](https://github.com/cogitatio/vagrant-hostsupdater) `vagrant plugin install vagrant-hostsupdater`
- Provision with `vagrant up`
- Uninstall with `vagrant destroy`

What you get..
--------------
- Key/value management [http://manager-01:8500/ui](http://manager-01:8500/ui)
- Docker container management [http://manager-01:8080/ui](http://manager-01:8080/ui)
- Docker Hub mirror for caching of images across the cluster.
- Container orchestration with docker swarm:
    ```
    $ export DOCKER_HOST=tcp://manager-01:4000
    $ cd dropins/nginx
    $ docker-compose up -d
    $ docker-compose scale app=3
    ```